require 'yaml'
require 'erb'
require 'open3'

def get_bool_env(key, default=false)
  ENV[key].nil? ? default : ENV[key].to_s.downcase == "true"
end

def get_env(key, default=nil)
  ENV[key].nil? ? default : ENV[key].to_s.sub(/^"/, '').sub(/"$/, '')
end

def relative(path)
  File.expand_path("../" + path, __FILE__)
end

ENV_VARS = %w(
  SERVICE_DIR BUILD_CMD DRYRUN DEBUG
)
SERVICE_DIR  = get_env "SERVICE_DIR", "/srv"
DRYRUN       = get_bool_env "DRYRUN", false
DEBUG        = get_bool_env "DEBUG", false

SITE_WATCHER         = relative("site-watcher")
GIT_POLLER           = relative("git-poller")
WEB_SERVER           = relative("web-server")

SUPPRESS_OUTPUT = [
  /installing your bundle as root will break this application for all non-root/,
  /users on this machine/,
  /Don't run Bundler as root/,
  /non-root users on this machine/,
  /to see where a bundled gem is installed/,
  /Auto-regeneration: disabled/,
  /Plugins selected:/
]

def setup
  system("mkdir", "-p", SERVICE_DIR)
end

def git_clone(domain, site, prefix: "GIT")
  Dir.chdir(SERVICE_DIR) do
    dest = File.join(SERVICE_DIR, domain)
    if File.exist?(dest)
      Dir.chdir(domain) do
        run "git", "reset", "--hard", prefix: prefix
        run "git", "pull", prefix: prefix
      end
    else
      run "git", "clone", site['git'], dest, prefix: prefix
    end
  end
end

def git_pull(domain, site, prefix: "GIT")
  Dir.chdir(File.join(SERVICE_DIR, domain)) do
    run "git", "fetch", prefix: prefix
    run "git", "merge", prefix: prefix
  end
end

def bundle_install(domain, site, prefix: "BUNDLE")
  Dir.chdir(File.join(SERVICE_DIR, domain)) do
    if File.exist?("Gemfile.lock")
      #run "rm", "Gemfile.lock", prefix: prefix
    end
    # set bundler to use jobs equal to one less than the number of CPUs
    run "BUNDLE_USER_HOME=/srv bundle config --global jobs $(expr $(grep -c processor /proc/cpuinfo) - 1)", prefix: prefix
    run "mkdir", "-p", "/srv/gems", prefix: prefix
    run "BUNDLE_USER_HOME=/srv GEM_HOME=/srv/gems bundle", prefix: prefix
  end
end

def bundle_update(domain, site, prefix: "BUNDLE")
  Dir.chdir(File.join(SERVICE_DIR, domain)) do
    run "BUNDLE_USER_HOME=/srv GEM_HOME=/srv/gems bundle update", prefix: prefix
  end
end

def jekyll_build(domain, site, prefix: "JEKYLL")
  Dir.chdir(File.join(SERVICE_DIR, domain)) do
    cmd = "GEM_HOME=/srv/gems #{site["build"]}"
    if site["build"] == "bundle exec jekyll build"
      cmd << " --destination ./#{site["root"]}"
    end
    run cmd, prefix: prefix
  end
end

def parse_env(verbose: false)
  sites = {}
  i = 0
  while true do
    site = get_env("SITE_#{i}")
    if site.nil?
      break
    else
      sites[site] = {
        "git"     => get_env("SITE_#{i}_GIT"),
        "domains" => get_env("SITE_#{i}_DOMAINS", "").split(',').map(&:strip),
        "root"    => get_env("SITE_#{i}_ROOT", "public"),
        "build"   => get_env("SITE_#{i}_BUILD", "bundle exec jekyll build")
      }
    end
    i+=1
  end

  if sites.size == 0
    output "ERROR: environment variable SITE_0 is not defined"
    exit
  end

  if verbose
    output "SETUP: environment variables SITE_X parse as:"
    sites.to_yaml.split("\n").each do |line|
      output line, "SETUP"
    end
  end
  return sites
rescue StandardError => exc
  output exc.to_s, "SETUP"
  output "Could not parse environment variable SITE_X", "SETUP"
  output "SITE_X should be in the form:", "SETUP"
  output "SITE_0=example.org SITE_0_GIT=https://github.org/example/example.org", "SETUP"
  exit
end

def env_var_array
  ENV_VARS.map {|varname|
    if ENV[varname]
      if ENV[varname].strip =~ /^".*"$/
        '%s=%s' % [varname, ENV[varname]]
      else
        '%s="%s"' % [varname, ENV[varname]]
      end
    end
  }.compact
end

def env_sites_array
  return_array = []
  i = 0
  while true do
    site = "SITE_#{i}"
    if ENV["SITE_#{i}"]
      return_array << "SITE_#{i}=\"#{ENV["SITE_#{i}"]}\""
      return_array << "SITE_#{i}_GIT=\"#{ENV["SITE_#{i}_GIT"]}\""
      return_array << "SITE_#{i}_DOMAINS=\"#{ENV["SITE_#{i}_DOMAINS"]}\""
      return_array << "SITE_#{i}_ROOT=\"#{ENV["SITE_#{i}_ROOT"]}\""
    else
      break
    end
    i+=1
  end
  return_array
end

def output(str, prefix=nil)
  if prefix
    STDOUT.puts "#{prefix}: #{str}"
  else
    STDOUT.puts str
  end
  STDOUT.flush
end

def run(*cmd)
  if cmd.last.is_a?(Hash)
    options = cmd.pop
  else
    options = {}
  end
  exit_status = -1
  prefix = "%s (%s)" % [options[:prefix], `pwd`.strip]
  output cmd.join(' '), prefix
  Open3.popen2e(ENV, *cmd) do |stdin, out, thread|
    while line = out.gets do
      unless !!SUPPRESS_OUTPUT.detect {|re| re.match line}
        output line, options[:prefix]
      end
    end
    exit_status = thread.value.exitstatus.to_i
  end
  if exit_status != 0
    output "ERROR", options[:prefix]
    exit exit_status
  end
end
